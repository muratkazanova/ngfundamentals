import { Component, OnInit, OnDestroy } from '@angular/core';
import { LoggerService } from '../Shared/logger.service';

@Component({
  selector: 'app-logged-messages',
  templateUrl: './logged-messages.component.html',
  styleUrls: ['./logged-messages.component.scss']
})
export class LoggedMessagesComponent implements OnInit, OnDestroy {
  constructor(private loggerService: LoggerService) {}

  ngOnInit() {
    this.loggerService.isLogVisible = true;
  }

  ngOnDestroy() {
    this.loggerService.isLogVisible = false;
  }

  toggleLogView() {
    this.loggerService.toggleLogView();
  }
}
