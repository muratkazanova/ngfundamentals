import { Component, OnInit } from '@angular/core';
import { LoginService } from './Shared/login.service';
import { IUser } from './models/iuser';
import { LoggerService } from './Shared/logger.service';
import {
  NavigationStart,
  NavigationEnd,
  NavigationError,
  NavigationCancel,
  Router
} from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  brandName = 'Angular Fundamentals';
  currentUser: IUser | null;
  isCollapsed = true;
  loading = false;
  isLogListWidgetAvailable = false;
  constructor(
    private loginService: LoginService,
    private loggerService: LoggerService,
    private router: Router
  ) {}

  ngOnInit() {
    this.loginService.loginStatus.subscribe((usr: IUser) => {
      this.currentUser = usr;
      if (usr != null) {
        this.router.navigate(['/employees']);
      } else {
        this.router.navigate(['/login']);
      }
    });

    this.router.events.subscribe(routerEvent => {
      if (routerEvent instanceof NavigationStart) {
        this.loading = true;
      }

      if (
        routerEvent instanceof NavigationEnd ||
        routerEvent instanceof NavigationError ||
        routerEvent instanceof NavigationCancel
      ) {
        console.log('end or cancel');
        if (routerEvent instanceof NavigationEnd) {
          this.loggerService.log(
            `Routed path: ${JSON.stringify(routerEvent.url)}`
          );
        }
        this.loading = false;
      }
    });
  }

  signOut() {
    this.loginService.logout();
  }

  toggleLogListWidget() {
    this.loggerService.toggleLogView();
  }
}
