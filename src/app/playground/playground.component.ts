import { Component, OnInit } from '@angular/core';
import { Instructor } from '../models/Instructor';
import { IUser } from '../models/iuser';
import { LoginService } from '../Shared/login.service';

@Component({
  selector: 'app-playground',
  templateUrl: './playground.component.html',
  styleUrls: ['./playground.component.scss']
})
export class PlaygroundComponent implements OnInit {
  firstName: string;
  lastName: string;
  gender: string;
  isHappy: boolean = true;
  imageSource = 'https://picsum.photos/200';
  search: string;
  instructors: Instructor[] = [];
  today = new Date();
  distanceInMiles = 37;
  currentUser: IUser | null;
  constructor(private loginService: LoginService) {
    this.firstName = 'Murat';
    this.lastName = 'Kazanova';
    let imageNo = 0;
    let id = setInterval(() => {
      if (imageNo < 10) {
        imageNo++;
        this.imageSource = `https://picsum.photos/200?random&${imageNo}`;
      } else {
        clearInterval(id);
      }
    }, 3000);
    this.instructors = [
      new Instructor('Murat', 'Kazanova', 43, 'male'),
      new Instructor('Samantha', ' Hopkins', 34, 'female')
    ];
  }

  ngOnInit() {
    this.loginService.loginStatus.subscribe((usr: IUser) => {
      this.currentUser = usr;
    });
  }

  getMessage(): string {
    return 'Hello world';
  }

  onInputHandler(event: Event) {
    this.firstName = (<HTMLInputElement>event.target).value;
  }

  onChangeHandler(value: string) {
    this.firstName = value;
  }
}
