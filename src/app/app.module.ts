import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {
  NgbModule,
  NgbAccordionModule,
  NgbTabsetModule
} from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import { PlaygroundComponent } from './playground/playground.component';
import { EmployeeDashboardModule } from './employee-dashboard/employee-dashboard.module';
import { LifeCycleHooksModule } from './life-cycle-hooks/life-cycle-hooks.module';
import { SharedModule } from './Shared/shared.module';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { AppRoutingModule } from './/app-routing.module';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { LoggedMessagesComponent } from './logged-messages/logged-messages.component';

@NgModule({
  declarations: [
    AppComponent,
    PlaygroundComponent,
    LoginComponent,
    HomeComponent,
    PageNotFoundComponent,
    LoggedMessagesComponent
  ],
  imports: [
    BrowserModule,
    EmployeeDashboardModule,
    AppRoutingModule,
    SharedModule,
    NgbModule,
    NgbAccordionModule,
    NgbTabsetModule,
    LifeCycleHooksModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
