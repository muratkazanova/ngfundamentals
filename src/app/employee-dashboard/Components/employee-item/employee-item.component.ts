import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { IEmployee } from '../../models/iemployee';
import { EmployeeService } from '../../Services/employee.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-employee-item',
  templateUrl: './employee-item.component.html',
  styleUrls: ['./employee-item.component.scss']
})
export class EmployeeItemComponent implements OnInit {
  @Input()
  employee: IEmployee;
  isEdit = false;
  @ViewChild('edtFirstName')
  inputFirstName: ElementRef;
  @ViewChild('edtLastName')
  inputLastName: ElementRef;
  detailStyle = '';
  constructor(
    private employeeService: EmployeeService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.route.queryParamMap.subscribe(params => {
      this.detailStyle = params.get('detailStyle') || 'inline';
    });
  }

  onEdit() {
    if (this.detailStyle === 'inline') {
      this.isEdit = true;
    } else {
      this.router.navigate(['/employees', this.employee.id], {
        queryParams: {
          detailStyle: this.detailStyle
        }
        // relativeTo: this.route
      });
    }
  }
  onEmployeeUpdate(FirstName, LastName, Id) {
    this.employeeService.updateEmployee(FirstName, LastName, Id).subscribe(
      v => {
        console.log(v);
        this.isEdit = false;
      },
      error => console.log(error)
    );
  }

  onEmployeeDelete(id: number) {
    this.employeeService
      .deleteEmployee(id)
      .subscribe(v => console.log('deleted'), e => console.log(e));
  }

  resetTemplate() {
    this.isEdit = false;
    this.inputFirstName.nativeElement.value = this.employee.FirstName;
    this.inputLastName.nativeElement.value = this.employee.LastName;
  }
}
