import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IEmployee } from '../../models/iemployee';
import { EmployeeService } from '../../Services/employee.service';
import { tap, switchMap } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-employee-taskbar',
  templateUrl: './employee-taskbar.component.html',
  styleUrls: ['./employee-taskbar.component.scss']
})
export class EmployeeTaskbarComponent implements OnInit {
  constructor(
    private employeeService: EmployeeService,
    private router: Router,
    private route: ActivatedRoute
  ) {}
  employees: IEmployee[];
  detailStyle = 'inline';

  ngOnInit() {
    this.route.queryParamMap.subscribe(params => {
      if (params.has('detailStyle')) {
        this.detailStyle = params.get('detailStyle');
      }
    });
    this.employeeService.EmployeeSourceChanges.pipe(
      switchMap(v => this.employeeService.getEmployees())
    ).subscribe(employees => {
      this.employees = employees;
    });
  }

  onNewEmployee() {
    this.employeeService.toggleEmployeeForm(true);
  }

  onDetailStyleChange(value: string) {
    this.detailStyle = value;
    this.router.navigate(['/employees'], {
      queryParams: {
        detailStyle: value
      }
    });
  }
}
