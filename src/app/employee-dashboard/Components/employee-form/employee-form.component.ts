import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { IEmployee } from '../../models/iemployee';
import { EmployeeService } from '../../Services/employee.service';

@Component({
  selector: 'app-employee-form',
  templateUrl: './employee-form.component.html',
  styleUrls: ['./employee-form.component.scss']
})
export class EmployeeFormComponent implements OnInit {
  @ViewChild('firstname')
  firstName: ElementRef;
  constructor(private employeeService: EmployeeService) {}

  ngOnInit() {
    this.firstName.nativeElement.focus();
  }

  onNewEmployee(FirstName: string, LastName: string) {
    this.employeeService
      .newEmployee(FirstName, LastName)
      .subscribe(v => console.log(v), error => console.log(error));
  }

  onCancel() {
    this.employeeService.toggleEmployeeForm(false);
  }
}
