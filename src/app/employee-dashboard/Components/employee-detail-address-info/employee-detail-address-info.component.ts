import { Component, OnInit } from '@angular/core';
import { IAddress } from '../../models/iaddress';
import { IEmployee } from '../../models/iemployee';
import { ActivatedRoute } from '@angular/router';
import { EmployeeService } from '../../Services/employee.service';

@Component({
  selector: 'app-employee-detail-address-info',
  templateUrl: './employee-detail-address-info.component.html',
  styleUrls: ['./employee-detail-address-info.component.scss']
})
export class EmployeeDetailAddressInfoComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private employeeService: EmployeeService
  ) {}

  isAddressInfoEditMode = false;
  address: IAddress;
  ngOnInit() {
    this.route.parent.data.subscribe(data => {
      console.log(data);
      this.address = (data['employee'] as IEmployee).Addresses[0];
    });
    this.employeeService.AddressInfoEditModeChanges.subscribe(value => {
      this.isAddressInfoEditMode = value;
    });
  }

  compareAddressLine(currentValue: string) {
    if (currentValue !== this.address.AddressLine1) {
      this.employeeService.isAddressChanged = true;
    } else {
      this.employeeService.isAddressChanged = false;
    }
  }
}
