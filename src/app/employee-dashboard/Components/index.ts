export * from './employee-detail/employee-detail.component';
export * from './employee-form/employee-form.component';
export * from './employee-item/employee-item.component';
export * from './employee-list/employee-list.component';
export * from './employee-taskbar/employee-taskbar.component';
export * from './employee-detail-basic-info/employee-detail-basic-info.component';
export * from './employee-detail-address-info/employee-detail-address-info.component';
