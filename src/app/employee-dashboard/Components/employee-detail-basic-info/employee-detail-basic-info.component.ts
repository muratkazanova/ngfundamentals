import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IEmployee } from '../../models/iemployee';

@Component({
  selector: 'app-employee-detail-basic-info',
  templateUrl: './employee-detail-basic-info.component.html',
  styleUrls: ['./employee-detail-basic-info.component.scss']
})
export class EmployeeDetailBasicInfoComponent implements OnInit {

  employee: IEmployee;
  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.parent.data.subscribe(data => {
      this.employee = data['employee'];
      console.log(data);
    });
  }
}
