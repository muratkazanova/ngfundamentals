import { Component, OnInit, Input } from '@angular/core';
import { IEmployee } from '../../models/iemployee';
import { EmployeeService } from '../../Services/employee.service';
import { switchMap } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.scss']
})
export class EmployeeListComponent implements OnInit {
  employees: IEmployee[];
  constructor(
    private employeeService: EmployeeService,
    private route: ActivatedRoute
  ) {}
  ngOnInit() {
    // this.employeeService.EmployeeSourceChanges.pipe(
    //   switchMap(v => this.employeeService.getEmployees())
    // ).subscribe(employees => {
    //   this.employees = employees;
    // });

    this.route.data.subscribe(data => {
      console.log(data);
      this.employees = data['employees'];
    });
  }
}
