import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../../Services/employee.service';
import { ActivatedRoute, Router } from '@angular/router';
import { IEmployee } from '../../models/iemployee';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee-detail.component.scss']
})
export class EmployeeDetailComponent implements OnInit {
  employee: any;
  constructor(
    private employeeService: EmployeeService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    // this.employee = this.route.paramMap.pipe(
    //   switchMap(params => {
    //     const id: number | null = +params.get('id') || null;
    //     if (id) {
    //       return this.employeeService.getEmployee(id);
    //     } else {
    //       return of(null);
    //     }
    //   })
    // );
  }

  onAddressClick() {
    this.router.navigate(['addressInfo'], {
      relativeTo: this.route,
      queryParamsHandling: 'preserve'
    });
  }

  onAddressInfoEdit() {
    this.employeeService.AddressInfoEditModeChanges.next(true);
  }
}
