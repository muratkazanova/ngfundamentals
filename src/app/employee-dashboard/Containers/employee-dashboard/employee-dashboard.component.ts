import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../../Services/employee.service';

@Component({
  selector: 'app-employee-dashboard',
  templateUrl: './employee-dashboard.component.html',
  styleUrls: ['./employee-dashboard.component.scss']
})
export class EmployeeDashboardComponent implements OnInit {
  isNewEmployee = false;
  constructor(private employeeService: EmployeeService) {}

  ngOnInit() {
    this.employeeService.EmployeeFormEvent.subscribe(value => {
      this.isNewEmployee = value;
    });
  }
}
