export interface IAddress {
  id: number;
  AddressLine1: string;
  AddressLine2: string;
  StateProvince: string;
  City: string;
  PostalCode: string;
  Country: string;
}
