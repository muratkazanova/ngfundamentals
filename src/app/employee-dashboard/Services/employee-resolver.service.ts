import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { IEmployee } from '../models/iemployee';
import { EmployeeService } from './employee.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeeResolver implements Resolve<IEmployee> {
  constructor(private employeeService: EmployeeService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<IEmployee> {
    const id: number | null = +route.paramMap.get('id') || null;
    console.log('id', id);
    return this.employeeService.getEmployee(id);
  }
}
