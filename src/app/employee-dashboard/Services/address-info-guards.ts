import { EmployeeDetailAddressInfoComponent } from './../components/employee-detail-address-info/employee-detail-address-info.component';
import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivate,
  CanDeactivate
} from '@angular/router';

import { EmployeeService } from './employee.service';
import { LoginService } from 'src/app/Shared/login.service';
import { LoggerService } from 'src/app/Shared/logger.service';

@Injectable({
  providedIn: 'root'
})
export class AddressInfoGuard
  implements CanActivate, CanDeactivate<EmployeeDetailAddressInfoComponent> {
  constructor(
    private loginService: LoginService,
    private loggerService: LoggerService,
    private employeeService: EmployeeService
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (
      this.loginService.currentUser &&
      this.loginService.currentUser.isAdmin === true
    ) {
      this.loggerService.log(
        'AddressInfoGuard is applied, user is authorized to see address info.'
      );
      return true;
    } else {
      this.loggerService.log(
        'AddressInfoGuard is applied, user is not authorized to see address info.'
      );
      return false;
    }
  }

  canDeactivate(
    component: EmployeeDetailAddressInfoComponent,
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) {
    if (this.employeeService.isAddressChanged) {
      alert(`Address changed, you can't navigate`);
      return false;
    } else {
      return true;
    }
  }
}
