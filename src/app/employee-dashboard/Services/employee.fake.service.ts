import { Injectable, EventEmitter, Output } from '@angular/core';
import { IEmployee } from '../models/iemployee';

@Injectable({
  providedIn: 'root'
})
export class EmployeeFakeService {
  public employees: IEmployee[] = [
    {
      id: 1,
      FirstName: 'Murat',
      LastName: 'Kazanova'
    },
    {
      id: 2,
      FirstName: 'Trung',
      LastName: 'Ta'
    }
  ];
  @Output()
  EmployeesEvent: EventEmitter<IEmployee[]> = new EventEmitter();
  @Output()
  EmployeeFormEvent: EventEmitter<boolean> = new EventEmitter();
  constructor() {}

  getEmployees() {
    return [...this.employees];
  }
  updateEmployee(FirstName, LastName, id) {
    this.employees = this.employees.map(item => {
      if (item.id !== id) {
        return item;
      }
      return Object.assign({}, item, {
        FirstName,
        LastName
      });
    });
    this.EmployeesEvent.emit([...this.employees]);
  }

  deleteEmployee(id: number) {
    this.employees = this.employees.filter(e => e.id !== id);
    this.EmployeesEvent.emit([...this.employees]);
  }

  newEmployee(FirstName: string, LastName: string) {
    const id = this.employees.length + 1;
    this.employees = [...this.employees, { id, FirstName, LastName }];
    this.EmployeesEvent.emit([...this.employees]);
    this.toggleEmployeeForm(false);
  }

  toggleEmployeeForm(status: boolean) {
    this.EmployeeFormEvent.emit(status);
  }
}
