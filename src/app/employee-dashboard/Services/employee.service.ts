import { Injectable, EventEmitter, Output } from '@angular/core';
import { IEmployee } from '../models/iemployee';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { tap, switchMap, delay } from 'rxjs/operators';
import { Router } from '@angular/router';
import { LoggerService } from 'src/app/Shared/logger.service';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  private apiUrl = 'http://localhost:3000/api';
  private employees: IEmployee[] = [];

  rl: number; // Reload
  isAddressChanged = false;
  EmployeeSourceChanges = new BehaviorSubject<string>('initial');
  AddressInfoEditModeChanges = new BehaviorSubject<boolean>(false);

  @Output()
  EmployeeFormEvent: EventEmitter<boolean> = new EventEmitter();
  constructor(
    private http: HttpClient,
    private router: Router,
    private loggerService: LoggerService
  ) {
    this.EmployeeSourceChanges.subscribe(() => {
      // Change querystring parameter so Data Resolver can get triggered.
      this.rl = this.rl === 0 ? 1 : 0;
      this.router.navigate(['/employees'], {
        queryParams: { rl: this.rl },
        queryParamsHandling: 'merge'
      });
    });
  }

  getEmployees(): Observable<IEmployee[]> {
    const url = `${this.apiUrl}/employees`;
    return this.http.get<IEmployee[]>(url);
  }

  getDelayedEmployees(): Observable<IEmployee[]> {
    const url = `${this.apiUrl}/employees`;
    return this.http.get<IEmployee[]>(url).pipe(delay(1000));
  }

  getEmployee(id: number): Observable<IEmployee> {
    const url = `${this.apiUrl}/employees/${id}?_embed=Addresses`;
    return this.http.get<IEmployee>(url);
  }
  updateEmployee(FirstName: string, LastName: string, id: number) {
    const url = `${this.apiUrl}/employees/${id}`;
    return this.http.get<IEmployee>(url).pipe(
      switchMap(employee => {
        return this.http
          .put<IEmployee>(
            url,
            Object.assign({}, employee, {
              FirstName,
              LastName
            })
          )
          .pipe(
            tap(v => {
              this.EmployeeSourceChanges.next('EmployeeUpdate');
              this.loggerService.log('Employee is updated.');
            })
          );
      })
    );
  }

  deleteEmployee(id: number) {
    const url = `${this.apiUrl}/employees/${id}`;
    return this.http.delete(url).pipe(
      tap(v => {
        this.EmployeeSourceChanges.next('EmployeeUpdate');
        this.loggerService.log(`Employee (Id ${id}) deleted.`);
      })
    );
  }

  newEmployee(FirstName: string, LastName: string) {
    const url = `${this.apiUrl}/employees`;
    return this.http.post(url, { FirstName, LastName }).pipe(
      tap(v => {
        this.toggleEmployeeForm(false);
        this.loggerService.log(
          `New Employee, ${FirstName} ${LastName} created`
        );
        this.EmployeeSourceChanges.next('new employee');
      })
    );
  }

  toggleEmployeeForm(status: boolean) {
    this.EmployeeFormEvent.emit(status);
  }
}
