import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  EmployeeDashboardComponent,
  EmployeeTaskbarComponent,
  EmployeeFormComponent,
  EmployeeListComponent,
  EmployeeItemComponent,
  EmployeeDetailComponent,
  EmployeeDetailBasicInfoComponent,
  EmployeeDetailAddressInfoComponent
} from './index';
import { SharedModule } from '../Shared/shared.module';
import { EmployeeRoutingModule } from './employee-routing.module';

@NgModule({
  imports: [CommonModule, SharedModule, EmployeeRoutingModule],
  declarations: [
    EmployeeDashboardComponent,
    EmployeeTaskbarComponent,
    EmployeeListComponent,
    EmployeeItemComponent,
    EmployeeFormComponent,
    EmployeeDetailComponent,
    EmployeeDetailBasicInfoComponent,
    EmployeeDetailAddressInfoComponent
  ],
  exports: [EmployeeDashboardComponent]
})
export class EmployeeDashboardModule {}
