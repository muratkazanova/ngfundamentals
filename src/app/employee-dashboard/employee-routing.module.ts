import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  RouterModule,
  Routes,
  CanActivate,
  CanDeactivate
} from '@angular/router';
import {
  EmployeeDashboardComponent,
  EmployeeListComponent,
  EmployeeDetailComponent,
  EmployeeDetailBasicInfoComponent,
  EmployeeDetailAddressInfoComponent
} from './index';

import { EmployeeListResolver } from './Services/employee-list-resolver.service';
import { EmployeeResolver } from './Services/employee-resolver.service';
import { AddressInfoGuard } from './Services/address-info-guards';

const employeeRoutes: Routes = [
  {
    path: 'employees',
    component: EmployeeDashboardComponent,
    children: [
      {
        path: '',
        component: EmployeeListComponent,
        runGuardsAndResolvers: 'always',
        resolve: { employees: EmployeeListResolver }
      },
      {
        path: ':id',
        component: EmployeeDetailComponent,
        resolve: { employee: EmployeeResolver },
        children: [
          { path: '', redirectTo: 'basicInfo', pathMatch: 'full' },
          { path: 'basicInfo', component: EmployeeDetailBasicInfoComponent },
          {
            path: 'addressInfo',
            component: EmployeeDetailAddressInfoComponent,
            canActivate: [AddressInfoGuard],
            canDeactivate: [AddressInfoGuard]
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(employeeRoutes)],
  declarations: [],
  exports: [RouterModule]
})
export class EmployeeRoutingModule {}
