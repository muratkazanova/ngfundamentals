export class Instructor {
  constructor(
    public FirstName: string,
    public LastName: string,
    public Age: number,
    public Gender: string
  ) {}

  getFullName() {
    return `${this.FirstName} ${this.LastName}`;
  }
}
