export interface IUser {
  id: number;
  Name: string;
  isAdmin: boolean;
}
