import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-sample-parent',
  templateUrl: './sample-parent.component.html',
  styleUrls: ['./sample-parent.component.scss']
})
export class SampleParentComponent implements OnInit {
  ClassName = '';
  @ViewChild('classname')
  classname: ElementRef;
  constructor() {}

  ngOnInit() {}

  onInputHandler(classname: string) {
    this.ClassName = classname;
  }

  onClassNameUpdate(classname: string) {
    this.ClassName = classname;
    //this.classname.nativeElement.value = classname;
  }
}
