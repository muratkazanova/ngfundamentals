import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  SimpleChanges,
  OnChanges
} from '@angular/core';

@Component({
  selector: 'app-sample-child',
  templateUrl: './sample-child.component.html',
  styleUrls: ['./sample-child.component.scss']
})
export class SampleChildComponent implements OnInit, OnChanges {
  @Input()
  ClassName: string;
  @Output()
  UpdateClassName: EventEmitter<string> = new EventEmitter();
  constructor() {}

  ngOnInit() {
    console.log('Sample Child component OnInit');
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.ClassName) {
      console.log('Sample Child component OnChanges');
      console.log(
        `Old value: ${changes.ClassName.previousValue}, Current value: ${
          changes.ClassName.currentValue
        }`
      );
    }
  }

  onUpdate() {
    this.UpdateClassName.emit('Updated ClassName');
  }
}
