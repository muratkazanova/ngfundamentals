import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SampleChildComponent } from './Components/sample-child/sample-child.component';
import { SampleParentComponent } from './Components/sample-parent/sample-parent.component';
import { SharedStylesHost } from '@angular/platform-browser/src/dom/shared_styles_host';
import { SharedModule } from '../Shared/shared.module';

@NgModule({
  imports: [CommonModule, SharedModule],
  declarations: [SampleParentComponent, SampleChildComponent],
  exports: [SampleParentComponent]
})
export class LifeCycleHooksModule {}
