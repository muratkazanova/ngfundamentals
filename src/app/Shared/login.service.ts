import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { IUser } from '../models/iuser';
import { LoggerService } from './logger.service';
@Injectable({
  providedIn: 'root'
})
export class LoginService {
  constructor(private loggerService: LoggerService) {}

  loginStatus = new Subject<IUser | null>();
  currentUser?: IUser | null;
  login(userName: string, password: string): void {
    if (userName === 'admin' && password === 'pwd!2018') {
      this.currentUser = {
        id: 1,
        Name: userName,
        isAdmin: true
      };
      this.loggerService.log('Admin login');
    } else if (
      userName.toLowerCase() === 'intro2nguser' &&
      password === 'pwd!2018'
    ) {
      this.currentUser = {
        id: 2,
        Name: userName,
        isAdmin: false
      };
      this.loggerService.log(`User ${userName} successfully signed in.`);
    } else {
      this.loggerService.log(`Failed to sign in.`);
    }
    this.loginStatus.next(this.currentUser);
  }

  logout(): void {
    this.currentUser = null;
    this.loginStatus.next(this.currentUser);
    this.loggerService.log('User signed out..');
  }
}
