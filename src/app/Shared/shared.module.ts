import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MileToKmPipe } from './mile-to-km.pipe';
import { HighlightDirective } from './Directives/highlight.directive';
import { CustomIfDirective } from './Directives/custom-if.directive';

@NgModule({
  imports: [CommonModule, FormsModule, HttpClientModule],
  declarations: [MileToKmPipe, HighlightDirective, CustomIfDirective],

  exports: [
    HttpClientModule,
    FormsModule,
    MileToKmPipe,
    HighlightDirective,
    CustomIfDirective
  ]
})
export class SharedModule {}
