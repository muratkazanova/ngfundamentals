import { Directive, Input, ViewContainerRef, TemplateRef } from '@angular/core';

@Directive({
  selector: '[appCustomIf]'
})
export class CustomIfDirective {
  @Input('appCustomIf')
  set customIf(condition: boolean) {
    if (condition) {
      this.view.createEmbeddedView(this.template);
    } else {
      this.view.clear();
    }
  }
  constructor(
    private view: ViewContainerRef,
    private template: TemplateRef<any>
  ) {}
}
