import {
  Directive,
  ElementRef,
  Renderer2,
  HostBinding,
  HostListener,
  Input,
  OnInit
} from '@angular/core';
@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective implements OnInit {
  @Input('appHighlight')
  highlightColor = '';
  @Input()
  defaultColor = '';
  constructor(private el: ElementRef, private renderer: Renderer2) {}

  @HostBinding('style.backgroundColor')
  bgColor;

  @HostListener('mouseenter')
  onMouseOver() {
    this.bgColor = this.highlightColor || 'yellow';
  }

  @HostListener('mouseleave')
  onMouseLeave() {
    this.bgColor = this.defaultColor;
  }

  ngOnInit() {
    this.bgColor = this.defaultColor;
  }
}
