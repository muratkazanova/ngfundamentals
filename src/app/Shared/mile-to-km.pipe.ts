import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'mileToKm'
})
export class MileToKmPipe implements PipeTransform {
  transform(value: number, args?: any): number {
    return value * 1.609344;
  }
}
