import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoggerService {
  constructor(private router: Router) {}

  isLogVisible = false;
  messages: string[] = [];
  log(message: string) {
    console.log(message);
    this.messages.unshift(`${message} ${new Date().toLocaleString()}`);
  }

  toggleLogView() {
    this.isLogVisible = !this.isLogVisible;
    if (this.isLogVisible) {
      this.router.navigate([{ outlets: { logListWidget: ['logs'] } }]);
    } else {
      this.router.navigate([{ outlets: { logListWidget: null } }]);
    }
  }
}
